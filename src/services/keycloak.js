import Keycloak from "keycloak-js";

const keycloak = new Keycloak({
  url: "https://auth.mti12.dcm.ar/auth/",
  realm: "MTI",
  clientId: "mti12",
});

export default keycloak;
